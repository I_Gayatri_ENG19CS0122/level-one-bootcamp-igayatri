//Write a program to add two user input numbers using 4 functions.#include <stdio.h>
int input()
{
    float a;
    printf("Enter numbers:");
    scanf("%f",&a);
    return a;
}
int find_sum(float a,float b)
{
    float sum;
    sum=a+b; 
    return sum;
}
void output(float a,float b,float sum)
{
    printf("Sum is %f",sum);
 }
int main()
{
   float x,y,s;
    x=input();
    y=input();
    s=find_sum(x,y);
    output(x,y,s);
    return 0;
}
